package br.ufsc.brocodeufsc.aghelloworld.model;

import java.util.Random;

import br.ufsc.brocodeufsc.aghelloworld.engine.Algoritmo;

public class Individuo {

	private String genes = "";
	private int aptidao = 0;

	// gera um indivíduo aleatório
	public Individuo(final int numGenes) {
		genes = "";
		final Random r = new Random();

		final String caracteres = Algoritmo.getCaracteres();

		for(int i = 0; i < numGenes; i++) {
			genes += caracteres.charAt(r.nextInt(caracteres.length()));
		}

		geraAptidao();
	}

	// cria um indivíduo com os genes definidos
	public Individuo(final String genes) {
		this.genes = genes;

		final Random r = new Random();
		// se for mutar, cria um gene aleatório
		if(r.nextDouble() <= Algoritmo.getTaxaDeMutacao()) {
			final String caracteres = Algoritmo.getCaracteres();
			String geneNovo = "";
			final int posAleatoria = r.nextInt(genes.length());
			for(int i = 0; i < genes.length(); i++) {
				if(i == posAleatoria) {
					geneNovo += caracteres.charAt(r.nextInt(caracteres.length()));
				} else {
					geneNovo += genes.charAt(i);
				}

			}
			this.genes = geneNovo;
		}
		geraAptidao();
	}

	// gera o valor de aptidão, será calculada pelo número de bits do gene
	// iguais ao da solução
	private void geraAptidao() {
		final String solucao = Algoritmo.getSolucao();
		for(int i = 0; i < solucao.length(); i++) {
			if(solucao.charAt(i) == genes.charAt(i)) {
				aptidao++;
			}
		}
	}

	public int getAptidao() {
		return aptidao;
	}

	public String getGenes() {
		return genes;
	}

}
