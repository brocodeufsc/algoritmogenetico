package br.ufsc.brocodeufsc.aghelloworld.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Populacao {

	private final Individuo[] individuos;
	private final int tamPopulacao;

	// cria uma população com indivíduos aleatória
	public Populacao(final int numGenes, final int tamPop) {
		tamPopulacao = tamPop;
		individuos = new Individuo[tamPop];
		for(int i = 0; i < individuos.length; i++) {
			individuos[i] = new Individuo(numGenes);
		}
	}

	// cria uma população com indivíduos sem valor, será composto posteriormente
	public Populacao(final int tamPop) {
		tamPopulacao = tamPop;
		individuos = new Individuo[tamPop];
		for(int i = 0; i < individuos.length; i++) {
			individuos[i] = null;
		}
	}

	// coloca um indivíduo em uma certa posição da população
	public void addIndividuo(final Individuo individuo, final int posicao) {
		individuos[posicao] = individuo;
	}

	// coloca um indivíduo na próxima posição disponível da população
	public void addIndividuo(final Individuo individuo) {
		for(int i = 0; i < individuos.length; i++) {
			if(individuos[i] == null) {
				individuos[i] = individuo;
				return;
			}
		}
	}

	// coloca um par de indivíduos na próxima posição disponível da população
	public void addPar(final Par par) {
		addIndividuo(par.getIndividuo1());
		addIndividuo(par.getIndividuo2());
	}

	// verifoca se algum indivíduo da população possui a solução
	public boolean temSolocao(final String solucao) {
		for(final Individuo individuo : individuos) {
			if(individuo.getGenes().equals(solucao)) {
				return true;
			}
		}
		return false;
	}

	// ordena a população pelo valor de aptidão de cada indivíduo, do maior
	// valor para o menor, assim se eu quiser obter o melhor indivíduo desta
	// população, acesso a posição 0 do array de indivíduos
	public void ordenaPopulacao() {
		boolean trocou = true;
		while(trocou) {
			trocou = false;
			for(int i = 0; i < individuos.length - 1; i++) {
				if(individuos[i].getAptidao() < individuos[i + 1].getAptidao()) {
					final Individuo temp = individuos[i];
					individuos[i] = individuos[i + 1];
					individuos[i + 1] = temp;
					trocou = true;
				}
			}
		}
	}

	// número de indivíduos existentes na população
	public int getNumIndividuos() {
		int num = 0;
		for(final Individuo individuo : individuos) {
			if(individuo != null) {
				num++;
			}
		}
		return num;
	}

	public int getTamPopulacao() {
		return tamPopulacao;
	}

	public Individuo getIndividuo(final int pos) {
		return individuos[pos];
	}

	public List<Individuo> getIndividuos() {
		return new ArrayList<>(Arrays.asList(individuos));
	}

}
