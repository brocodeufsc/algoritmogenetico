package br.ufsc.brocodeufsc.aghelloworld.model;

public class Par {

	private Individuo individuo1;
	private Individuo individuo2;

	public Par() {}

	public Par(final Individuo individuo1, final Individuo individuo2) {
		this.individuo1 = individuo1;
		this.individuo2 = individuo2;
	}

	public Individuo getIndividuo1() {
		return individuo1;
	}

	public void setIndividuo1(final Individuo individuo1) {
		this.individuo1 = individuo1;
	}

	public Individuo getIndividuo2() {
		return individuo2;
	}

	public void setIndividuo2(final Individuo individuo2) {
		this.individuo2 = individuo2;
	}

}
