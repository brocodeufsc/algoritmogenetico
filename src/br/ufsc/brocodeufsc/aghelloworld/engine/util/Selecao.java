package br.ufsc.brocodeufsc.aghelloworld.engine.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import br.ufsc.brocodeufsc.aghelloworld.model.Individuo;
import br.ufsc.brocodeufsc.aghelloworld.model.Par;
import br.ufsc.brocodeufsc.aghelloworld.model.Populacao;

public enum Selecao {

	ROLETA {
		@Override
		public Par selecionarPais(final Populacao populacao) {
			class Roleta {

				private final List<Individuo> individuos;

				private Roleta(final Populacao populacao) {
					individuos = new ArrayList<>();

					final List<Individuo> p = populacao.getIndividuos();
					for(final Individuo individuo : p) {
						for(int i = individuo.getAptidao(); i >= 0; i--) {
							individuos.add(individuo);
						}
					}
				}

				private int size() {
					return individuos.size();
				}

				private Individuo get(final int index) {
					final Individuo individuo = individuos.remove(index);
					for(final Iterator<Individuo> iterator = individuos.iterator(); iterator.hasNext();) {
						final Individuo i = iterator.next();
						if(i.getAptidao() == individuo.getAptidao()) {
							iterator.remove();
						}
					}
					return individuo;
				}

			}

			final Random r = new Random();
			final Roleta roleta = new Roleta(populacao);

			return new Par(roleta.get(r.nextInt(roleta.size())), roleta.get(r.nextInt(roleta.size())));
		}
	},
	TORNEIO {
		@Override
		public Par selecionarPais(final Populacao populacao) {
			final Random r = new Random();
			final Populacao populacaoIntermediaria = new Populacao(3);

			// seleciona 3 indivíduos aleatóriamente na população
			populacaoIntermediaria.addIndividuo(populacao.getIndividuo(r.nextInt(populacao.getTamPopulacao())));
			populacaoIntermediaria.addIndividuo(populacao.getIndividuo(r.nextInt(populacao.getTamPopulacao())));
			populacaoIntermediaria.addIndividuo(populacao.getIndividuo(r.nextInt(populacao.getTamPopulacao())));

			// ordena a população
			populacaoIntermediaria.ordenaPopulacao();

			// seleciona os 2 melhores deste população
			return new Par(populacaoIntermediaria.getIndividuo(0), populacaoIntermediaria.getIndividuo(1));
		}
	};

	public abstract Par selecionarPais(Populacao populacao);

}
