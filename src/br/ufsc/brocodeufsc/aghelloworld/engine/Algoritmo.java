package br.ufsc.brocodeufsc.aghelloworld.engine;

import java.util.Random;

import br.ufsc.brocodeufsc.aghelloworld.engine.util.Selecao;
import br.ufsc.brocodeufsc.aghelloworld.model.Individuo;
import br.ufsc.brocodeufsc.aghelloworld.model.Par;
import br.ufsc.brocodeufsc.aghelloworld.model.Populacao;

public class Algoritmo {

	private static String solucao;
	private static double taxaDeCrossover;
	private static double taxaDeMutacao;
	private static String caracteres;
	private static Integer elitismo;
	private static Selecao metodoSelecao;

	public static Populacao novaGeracao(final Populacao populacao) {
		final Random r = new Random();
		// nova população do mesmo tamanho da antiga
		final Populacao novaPopulacao = new Populacao(populacao.getTamPopulacao());

		// se tiver elitismo, mantém a quantidade desejada de melhores
		// indivíduos da geração atual
		for(int i = 0; getElitismo() != null && i < getElitismo(); i++) {
			novaPopulacao.addIndividuo(populacao.getIndividuo(i));
		}

		// insere novos indivíduos na nova população, até atingir o tamanho
		// máximo
		while(novaPopulacao.getNumIndividuos() < novaPopulacao.getTamPopulacao()) {
			// seleciona os 2 pais por torneio
			final Par pais = getMetodoSelecao().selecionarPais(populacao);

			final Par filhos;

			// verifica a taxa de crossover, se sim realiza o crossover, se não,
			// mantém os pais selecionados para a próxima geração
			if(r.nextDouble() <= taxaDeCrossover) {
				filhos = crossover(pais.getIndividuo1(), pais.getIndividuo2());
			} else {
				filhos = new Par(new Individuo(pais.getIndividuo1().getGenes()), new Individuo(pais.getIndividuo2().getGenes()));
			}

			// adiciona os filhos na nova geração
			novaPopulacao.addPar(filhos);
		}

		// ordena a nova população
		novaPopulacao.ordenaPopulacao();
		return novaPopulacao;
	}

	public static Par crossover(final Individuo individuo1, final Individuo individuo2) {
		final Random r = new Random();

		// sorteia o ponto de corte
		final int pontoCorte1 = r.nextInt(individuo1.getGenes().length() / 2 - 2) + 1;
		final int pontoCorte2 = r.nextInt(individuo1.getGenes().length() / 2 - 2) + individuo1.getGenes().length() / 2;

		// pega os genes dos pais
		final String genePai1 = individuo1.getGenes();
		final String genePai2 = individuo2.getGenes();

		String geneFilho1;
		String geneFilho2;

		// realiza o corte,
		geneFilho1 = genePai1.substring(0, pontoCorte1);
		geneFilho1 += genePai2.substring(pontoCorte1, pontoCorte2);
		geneFilho1 += genePai1.substring(pontoCorte2, genePai1.length());

		geneFilho2 = genePai2.substring(0, pontoCorte1);
		geneFilho2 += genePai1.substring(pontoCorte1, pontoCorte2);
		geneFilho2 += genePai2.substring(pontoCorte2, genePai2.length());

		// cria o novo indivíduo com os genes dos pais
		return new Par(new Individuo(geneFilho1), new Individuo(geneFilho2));
	}

	public static String getSolucao() {
		return solucao;
	}

	public static void setSolucao(final String solucao) {
		Algoritmo.solucao = solucao;
	}

	public static double getTaxaDeCrossover() {
		return taxaDeCrossover;
	}

	public static void setTaxaDeCrossover(final double taxaDeCrossover) {
		Algoritmo.taxaDeCrossover = taxaDeCrossover;
	}

	public static double getTaxaDeMutacao() {
		return taxaDeMutacao;
	}

	public static void setTaxaDeMutacao(final double taxaDeMutacao) {
		Algoritmo.taxaDeMutacao = taxaDeMutacao;
	}

	public static String getCaracteres() {
		return caracteres;
	}

	public static void setCaracteres(final String caracteres) {
		Algoritmo.caracteres = caracteres;
	}

	public static Integer getElitismo() {
		return elitismo;
	}

	public static void setElitismo(final Integer elitismo) {
		Algoritmo.elitismo = elitismo;
	}

	public static Selecao getMetodoSelecao() {
		return metodoSelecao;
	}

	public static void setMetodoSelecao(final Selecao metodoSelecao) {
		Algoritmo.metodoSelecao = metodoSelecao;
	}

}
