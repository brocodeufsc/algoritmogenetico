package br.ufsc.brocodeufsc.aghelloworld;

import java.text.MessageFormat;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;

import br.ufsc.brocodeufsc.aghelloworld.engine.Algoritmo;
import br.ufsc.brocodeufsc.aghelloworld.engine.util.Selecao;
import br.ufsc.brocodeufsc.aghelloworld.model.Individuo;
import br.ufsc.brocodeufsc.aghelloworld.model.Populacao;

public class AGhelloWorld {

	private static final class Padrao {

		private static final String CARACTERES = "!,.:;?áÁãÃâÂõÕôÔóÓéêÉÊíQWERTYUIOPASDFGHJKLÇZXCVBNMqwertyuiopasdfghjklçzxcvbnm1234567890 ";

		private static final String MAX_GERACOES = String.valueOf(10000);

		private static final String SOLUCAO = "Sistemas Inteligentes é a matéria mais divertida do curso de Sistemas de Informação!";
		private static final String TAXA_CROSSOVER = String.valueOf(0.6);
		private static final String TAXA_MUTACAO = String.valueOf(0.3);
		private static final String ELITISMO = String.valueOf(1);
	}

	public static void main(final String[] args) {
		// Define a solução
		Algoritmo.setSolucao(StringUtils.defaultIfBlank(JOptionPane.showInputDialog(MessageFormat.format("Solução\nPadrão: {0}", Padrao.SOLUCAO)), Padrao.SOLUCAO));

		// Define os caracteres existentes
		Algoritmo.setCaracteres(Padrao.CARACTERES);

		// taxa de crossover padrão de 60%
		Algoritmo.setTaxaDeCrossover(
			Double.parseDouble(StringUtils.defaultIfBlank(JOptionPane.showInputDialog("Taxa de Crossover\nPadrão: 0.6 (60%)"), Padrao.TAXA_CROSSOVER)));

		// taxa de mutação padrão de 3%
		Algoritmo.setTaxaDeMutacao(Double.parseDouble(StringUtils.defaultIfBlank(JOptionPane.showInputDialog("Taxa de Mutação\nPadrão: 0.3 (30%)"), Padrao.TAXA_MUTACAO)));

		// elitismo
		Algoritmo.setElitismo(
			Integer.parseInt(StringUtils.defaultIfBlank(JOptionPane.showInputDialog("Quantidade de indivíduos usados no elitismo\nPadrão: 1"), Padrao.ELITISMO)));

		final int option = JOptionPane.showOptionDialog(null, "Método de Seleção", "Entrada", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
			new String[] { "Torneio", "Roleta" }, 0);
		if(option != JOptionPane.CLOSED_OPTION) {
			Algoritmo.setMetodoSelecao(option == 0 ? Selecao.TORNEIO : Selecao.ROLETA);
		} else {
			Algoritmo.setMetodoSelecao(Selecao.TORNEIO);
		}

		// numero máximo de gerações
		final int numMaxGeracoes = Integer.parseInt(StringUtils.defaultIfBlank(JOptionPane.showInputDialog("Número máximo de gerações\nPadrão: 10000"), Padrao.MAX_GERACOES));

		// tamanho da população
		final int tamPop = 100;

		// define o número de genes do indivíduo baseado na solução
		final int numGenes = Algoritmo.getSolucao().length();

		// cria a primeira população aleatérioa
		Populacao populacao = new Populacao(numGenes, tamPop);

		boolean temSolucao;
		int geracao;

		System.out
			.println(MessageFormat.format(
				"Parâmetros:\nTaxa de Crossover:\t{0}\nTaxa de Mutação:\t{1}\nQuantidade de indivíduos para elitismo:\t{2}\nMétodo de Seleção:\t{3}\nSolução:\t{4}",
				Algoritmo.getTaxaDeCrossover(), Algoritmo.getTaxaDeMutacao(), Algoritmo.getElitismo(), Algoritmo.getMetodoSelecao(), Algoritmo.getSolucao()));

		System.out.println(MessageFormat.format("Iniciando... Aptidão da solução: {0}", Algoritmo.getSolucao().length()));

		// loop até o critério de parada
		Individuo melhor;
		for(geracao = 0, temSolucao = false; !temSolucao && geracao < numMaxGeracoes; geracao++, temSolucao = populacao.temSolocao(Algoritmo.getSolucao())) {
			// cria nova populacao
			populacao = Algoritmo.novaGeracao(populacao);
			melhor = populacao.getIndividuo(0);
			System.out.println(MessageFormat.format("Geração {0} | Aptidão: {1} | Melhor: {2}",
				geracao, melhor.getAptidao(), melhor.getGenes()));
		}

		melhor = populacao.getIndividuo(0);

		if(geracao == numMaxGeracoes) {
			System.out.println(MessageFormat.format("Número Maximo de Gerações | {0} {1}", melhor.getGenes(), melhor.getAptidao()));
		}

		if(temSolucao) {
			System.out.println(MessageFormat.format("Encontrado resultado na geração {0} | {1} (Aptidão: {2})",
				geracao, melhor.getGenes(), melhor.getAptidao()));
		}
	}

}
